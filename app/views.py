from django.shortcuts import render
from .models import Producto

# Create your views here.


def home(request):
    return render(request, 'app/home.html')


def clientes(request):
    return render(request, 'app/clientes.html')

def servicios(request):
    productos = Producto.objects.all()
    data = {
        'productos': productos
    }
    return render(request, 'app/servicios.html', data)

def contacto(request):
    return render(request, 'app/contacto.html')

def base(request):
    return render(request, 'app/base.html')

